<?php

/**
 * @file
 * Facebook Pull feed template.
 */
?>
<ul class="social-media-feed">
<?php foreach ($items as $item): ?>
  <li class="item">
    <div class="social-media-feed-message">
      <?php if (in_array($item->type, array('photo', 'video'))): ?>
      <div class="social-media-media">
        <img src=" <?php echo $item->picture; ?> "  alt="picture"/>
      </div>
      <?php endif; ?>
      <div class="social-media-feed-time">
        <?php echo t('!time ago.', array('!time' => format_interval(time() - strtotime($item->created_time)))); ?>
      </div>
      <?php if (isset($item->description)): ?>
      <div class="social-media-status-default">
        <?php if (strlen($item->description) > 100): ?>
          <?php echo substr($item->description, 0, 99) . "..."; ?>
        <?php else: ?>
          <?php echo $item->description; ?>
        <?php endif; ?>
      </div>
      <?php endif; ?>
      <?php if ($item->type === 'link'): ?>
        <?php if (isset($item->name)): ?>
          <?php echo l($item->name, $item->link); ?>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  </li>
<?php endforeach; ?>
</ul>
