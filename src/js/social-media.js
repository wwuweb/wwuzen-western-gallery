// From the social_media.js file
/**
 * Holds custom JavaScript for the Social Media panel in the wwu admissions front page.
 *
 */
(function ($) {

  "use strict";

  // Intialize feed posts.
  function init() {
    initPosts(".facebook-feed");
  }

  // Change arrow image according to its status.
  function changeArrow(arrowClass, arrowImage, arrowNewImage) {
    var source = $(arrowClass).attr("src");
    $(arrowClass).attr("src", source.replace( arrowImage, arrowNewImage));
  }

  // Add appropriate classes to a feed's posts for 3 column effect.
  function initPosts(feedName) {
    var cssArray = ["left-column", "center-column", "right-column"];
    var index = 0;

    $(feedName).children('li').each(function() {
      var cssIndex = index % 3;
      if(index > 2) {
        $(this).hide();
      }
      switch(cssIndex) {
        case 0:
          $(this).addClass( cssArray[cssIndex]);
          index++;
          break;
        case 1:
          $(this).addClass( cssArray[cssIndex]);
          index++;
          break;
        case 2:
          $(this).addClass( cssArray[cssIndex]);
          index++;
          break;
        default:
          break;
      }
    });
  } 


  // Go back to previous 3 posts for a feed.
  function prev(id, type) {
    var slideIndex = parseInt($("#"+id+"index").val());
    var length = $(type).children('li').length;
    var counter = slideIndex;

     if(slideIndex !== 0 && slideIndex < length) {
       counter = slideIndex+3;
    }
    else if ( slideIndex > length) {
      counter = length;
    }

    if(slideIndex !== 0) {
      for(var i = slideIndex-3; i < counter; i++) {
        if(i < slideIndex) {
          $(type).children('li').eq(i).css("display", "block");
        }
        else {
          $(type).children('li').eq(i).hide();
        }
      }
      slideIndex = slideIndex-3;
    }

    $("#"+id+"index").val(function(index, value) {
      return slideIndex;
    });
  }

  // Go forward to the next 3 posts for a feed.
  function next(id, type) {
    var slideIndex = parseInt($("#"+id+"index").val());
    var length = $(type).children('li').length;
    var counter = slideIndex;

    if(slideIndex+6 < length) {
      counter = slideIndex+6;
    }
    else if(slideIndex+3 < length) {
      counter= length;
    }

    if(slideIndex+3 < length) {
      for(var i = slideIndex; i < counter; i++) {
        if(i < slideIndex +3) {
          $(type).children('li').eq(i).hide();
        }
        else {
          $(type).children('li').eq(i).css("display", "block");
        }
      }

      slideIndex = slideIndex+3;
    }

    $("#"+id+"index").val(function(index, value) {
      return slideIndex;
    });
  }

  // Actions to take when arrow click event happens.
  function clickAction( arrowID, postID, action) {
    switch(action) {
      case "prev":
        prev(arrowID, postID);
        changeArrow("#prev" + arrowID, "left_arrow.png", "left_arrow_select.png");
        changeArrow("#next" + arrowID, "right_arrow_select.png", "right_arrow.png");
        break;
      case "next":
        next(arrowID, postID);
        changeArrow("#prev"+ arrowID, "left_arrow_select.png", "left_arrow.png");
        changeArrow("#next" + arrowID, "right_arrow.png", "right_arrow_select.png");
        break;
      default:
        break;
    }
  }

  // Set up buttons and  classes when page is loaded.
  $(document).ready( function() {
    init();
      alert("hello!");
    $("#prevFB").click(function() {
      clickAction("FB", ".facebook-feed", "prev");
    });

    $("#nextFB").click(function() {
      clickAction("FB", ".facebook-feed", "next");
    });

  });

})(jQuery);
