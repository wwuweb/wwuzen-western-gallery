(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.mainMenuOffset = {

    attach: function (context) {
      var $main_menu = $('#main-menu', context);
      $main_menu.css({
        bottom: -($main_menu.height() / 2),
      });
    }

  };

  Drupal.behaviors.facebookSlider = {
      attach: function () {
	new Western.Slider('.social-media-feed', {
	  'screen and (min-width: 800px)': 3,
	  'screen and (min-width: 400px) and (max-width: 799px)': 2,
	  'screen and (max-width: 399px)': 1
	});
    }

  };
}) (jQuery, Drupal, this, this.document);
